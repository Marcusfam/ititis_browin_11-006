import configparser
import requests
from urllib.parse import urlparse, urljoin, unquote
from bs4 import BeautifulSoup
import re

config = configparser.ConfigParser()
config.read("config.ini")

CORRECT_LINKS = []
ALL_SEEN_LINKS = []
LINKS_TO_CRAWL = []


def get_page(link):
    try:
        r = requests.get(link, timeout=5)
    except Exception:
        print("An Error occurred")
        print(unquote(link))
        return 0

    soup = BeautifulSoup(r.text, 'html.parser')

    return soup


def extract_links(link, soup):
    for a_tag in soup.find_all('a'):
        href = a_tag.get("href")

        href = urljoin(link, href)
        parsed_href = urlparse(href)

        href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path
        if href not in ALL_SEEN_LINKS:
            LINKS_TO_CRAWL.append(href)
            ALL_SEEN_LINKS.append(href)


def extract_text(soup):
    text = soup.get_text(separator=' ')
    text_of_page = re.sub(r'([^а-яА-ЯёЁ\s-])', '', text)
    print(text_of_page)
    text_of_page = re.sub(r'\s\s+', '\n', text_of_page)
    print(text_of_page)
    return text_of_page


def count_words(text):

    words = re.split(r'\s+-+|-+\s+|\s+', text)
    words = words[1:-1]

    # while '-' in words:
    #     words.remove('-')

    return len(words)


def main():
    main_url = str(config['task1']['main_url'])
    min_links = int(config['task1']['min_links_amount'])
    min_words = int(config['task1']['min_words_amount'])

    ALL_SEEN_LINKS.append(main_url)
    LINKS_TO_CRAWL.append(main_url)

    while len(CORRECT_LINKS) < min_links and len(LINKS_TO_CRAWL) > 0:
        cur_index = len(CORRECT_LINKS)
        cur_link = LINKS_TO_CRAWL.pop(0)
        soup = get_page(cur_link)
        if not soup:
            continue
        extract_links(cur_link, soup)

        text = extract_text(soup)
        words_amount = count_words(text)

        if words_amount >= min_words:
            CORRECT_LINKS.append(cur_link)

            file = open(config['task1']['main_file'], 'a', encoding="utf-8")
            file.write(f"{cur_index} {unquote(cur_link)} {words_amount} \n")
            file.close()

            file = open(f'{config["task1"]["pages_dir"]}/{cur_index}.txt', 'w', encoding="utf-8")
            file.write(text)
            file.close()


if __name__ == '__main__':
    main()

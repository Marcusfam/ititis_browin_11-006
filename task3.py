import re


def create_index():
    invert_index = {}
    full_index = {}
    for i in range(100):
        file = open(f'normalized_pages/{i}.txt', 'r', encoding="utf-8")
        text = file.read()
        file.close()
        text = re.split(r'[\s\n]', text)
        while '' in text:
            text.remove('')

        for word in text:
            if word in invert_index and i not in invert_index[word]:
                invert_index[word].append(i)
            else:
                invert_index[word] = [i]

            if word in full_index:
                if i in full_index[word]:
                    full_index[word][i] += 1
                else:
                    full_index[word][i] = 1
            else:
                full_index[word] = {i: 1}

    file = open('invert_index.txt', 'w', encoding="utf-8")
    file.write('')
    for key, item in dict(sorted(invert_index.items())).items():
        file.write(f"{key}: {item}\n")
    file.close()

    file = open('full_index.txt', 'w', encoding="utf-8")
    file.write('')
    for key, item in dict(sorted(full_index.items())).items():
        file.write(f"{key}: {item}\n")
    file.close()

    return invert_index, full_index


def find_words(prompt, invert_index):
    prompt = prompt.split(' ')
    print(f"{prompt}:")
    for word in range(len(prompt)):
        if prompt[word] not in ["&", "|"]:
            if prompt[word][0] != "!":
                prompt[word] = invert_index[prompt[word]]
            else:
                all_indexes = list(range(0, 101))
                prompt[word] = [value for value in all_indexes if value not in invert_index[prompt[word][1:]]]

    while "&" in prompt:
        index = prompt.index("&")
        result_list = [value for value in prompt[index - 1] if value in prompt[index + 1]]
        prompt[index] = result_list
        prompt.pop(index + 1)
        prompt.pop(index - 1)

    while "|" in prompt:
        index = prompt.index("|")
        result_list = list(set(prompt[index - 1] + prompt[index + 1]))
        prompt[index] = result_list
        prompt.pop(index + 1)
        prompt.pop(index - 1)

    print(prompt)
    print("\n")


def main():
    invert_index, full_index = create_index()
    prompts = ['сайт & превращаться | красивый', 'сайт | превращаться | красивый',
               'сайт & превращаться & красивый', 'сайт & !превращаться | !красивый',
               'сайт | !превращаться | !красивый']
    for prompt in prompts:
        find_words(prompt, invert_index)


if __name__ == '__main__':
    main()

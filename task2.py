import configparser
import re
import pymorphy2

config = configparser.ConfigParser()
config.read("config.ini")

morph = pymorphy2.MorphAnalyzer()


def tokenize_text(text):
    tokenized_text = []
    cur_word = ''
    for sym in text:
        if sym == ' ':
            tokenized_text.append(cur_word)
            tokenized_text.append(' ')
            cur_word = ''
        if sym == '\n':
            tokenized_text.append(cur_word)
            tokenized_text.append('\n')
            cur_word = ''
        else:
            cur_word += str(sym)
    # text_of_page = re.sub(r'([^а-яА-ЯёЁ-])', ' ', text)
    #
    # tokenized_text = re.split(r'\s+-+|-+\s+|\s+', text_of_page)

    return tokenized_text


def to_normal_form(text):
    for word in range(len(text)):
        text[word] = morph.parse(text[word])[0].normal_form

    return text


def delete_stop_words(text):
    stop_file = open(config["task2"]["stop_words"], 'r', encoding="utf-8")
    stop_words = stop_file.read().split('\n')
    stop_file.close()

    text_without_stop_words = []

    for word in text:
        if word not in stop_words:
            text_without_stop_words.append(word)

    return text_without_stop_words


def main():

    for i in range(0, int(config["task2"]["MIN_LINKS_AMOUNT"])):
        file = open(f'{config["task2"]["pages_dir"]}/{i}.txt', 'r', encoding="utf-8")
        text = file.read()
        file.close()

        text = tokenize_text(text)

        text = to_normal_form(text)

        text = delete_stop_words(text)

        file = open(f'{config["task2"]["normalized_pages_dir"]}/{i}.txt', 'w', encoding="utf-8")
        for word in text:
            file.write(f"{word}")
        file.close()
        print(i)


if __name__ == '__main__':
    main()

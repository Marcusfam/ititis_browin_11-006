import re
import math
import pandas as pd


def create_index():
    invert_index = {}
    full_index = {}
    documents_len = {}
    for i in range(100):
        file = open(f'normalized_pages/{i}.txt', 'r', encoding="utf-8")
        text = file.read()
        file.close()
        text = re.split(r'[\s\n]', text)
        while '' in text:
            text.remove('')

        documents_len[i] = 0
        for word in text:
            documents_len[i] += 1
            if word in invert_index and i not in invert_index[word]:
                invert_index[word].append(i)
            else:
                invert_index[word] = [i]

            if word in full_index:
                if i in full_index[word]:
                    full_index[word][i] += 1
                else:
                    full_index[word][i] = 1
            else:
                full_index[word] = {i: 1}

    file = open('invert_index.txt', 'w', encoding="utf-8")
    file.write('')
    for key, item in dict(sorted(invert_index.items())).items():
        file.write(f"{key}: {item}\n")
    file.close()

    file = open('full_index.txt', 'w', encoding="utf-8")
    file.write('')
    for key, item in dict(sorted(full_index.items())).items():
        file.write(f"{key}: {item}\n")
    file.close()

    return invert_index, full_index, documents_len


def count_tf(full_index, documents_len):
    tf_table = {}
    for key, value in full_index.items():
        tf_table[key] = []
        for document_key, document_value in documents_len.items():
            if document_key in value.keys():
                n = value[document_key]
            else:
                n = 0
            tf_table[key].append(round((n / document_value), 5))

    return tf_table


def count_idf(invert_index):
    idf_table = {}
    for key, value in invert_index.items():
        idf_table[key] = round(math.log(100 / len(value), 10), 5)

    return idf_table


def count_tf_idf(tf_table, idf_table):
    tf_idf_table = {}
    for key, values in tf_table.items():
        tf_idf_table[key] = []
        for value in values:
            tf_idf_table[key].append(round(value * idf_table[key], 5))

    return tf_idf_table


def main():
    invert_index, full_index, documents_len = create_index()
    tf_table = count_tf(full_index, documents_len)
    idf_table = count_idf(invert_index)
    tf_idf_table = count_tf_idf(tf_table, idf_table)

    df = pd.DataFrame.from_dict(dict(sorted(tf_table.items())), orient='index',
                           columns=[*range(0, 100)])
    df.to_excel('./tf_idf_tables/tf.xlsx')

    df = pd.DataFrame.from_dict(dict(sorted(idf_table.items())), orient='index',
                           columns=[0])
    df.to_excel('./tf_idf_tables/idf.xlsx')

    df = pd.DataFrame.from_dict(dict(sorted(tf_idf_table.items())), orient='index',
                           columns=[*range(0, 100)])
    df.to_excel('./tf_idf_tables/tf_idf.xlsx')


if __name__ == '__main__':
    main()
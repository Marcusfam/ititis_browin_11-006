import pandas as pd
import numpy
import math


def main():
    tf_idf = pd.read_excel('./tf_idf_tables/tf_idf.xlsx')
    tf_idf = tf_idf.to_numpy()
    words_list = tf_idf[:, 0]
    prompts = ["сайт", "сайт превращаться", "сайт превращаться красивый"]
    for prompt in prompts:
        prompt = prompt.split()
        prompt_vector = [1 if word in prompt else 0 for word in words_list]
        cosine_dict = {}
        file = open(f'task5_result/{prompt}.txt', 'w', encoding="utf-8")
        file.write(f"{prompt}\n")
        for i in range(100):
            dot_product = numpy.dot(prompt_vector, tf_idf[:, i + 1])
            magnitude = math.sqrt(sum(map(lambda x: x**2, prompt_vector)))*math.sqrt(numpy.sum(tf_idf[:, i+1]**2))
            cosine_similarity = dot_product/magnitude
            cosine_dict[f"{i}.txt"] = cosine_similarity

        for key, value in dict(sorted(cosine_dict.items(), key=lambda item: item[1], reverse=True)).items():
            file.write(f"{key}: {value}\n")


if __name__ == '__main__':
    main()
